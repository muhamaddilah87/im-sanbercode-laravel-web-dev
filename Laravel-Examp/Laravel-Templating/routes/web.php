<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/table', function () {
    return view('table');
});

Route::get('/data-tables', function () {
    return view('data-tables');
});

Route::get('/casts', [CastController::class, 'index']);
Route::get('/casts/create', [CastController::class, 'create']);
Route::post('/casts', [CastController::class, 'store']);
Route::get('/casts/{id}', [CastController::class, 'show']);
Route::get('/casts/{id}/edit', [CastController::class, 'edit']);
Route::put('/casts/{id}', [CastController::class, 'update']);
Route::delete('/casts/{id}', [CastController::class, 'destroy']);