<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('casts')->get();
        foreach ($cast as $value) {
            $value->bio = Str::limit($value->bio, 30);
            // return response()->json($value->bio);
        }
        return view('casts.casts', compact('cast'));
        // dd($cast);
    }

    public function create(){
        return view('casts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);
        $cast = DB::table('casts')->insert([
            'name' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        if($cast){
            Session::flash('status','success');
            Session::flash('message','Berhasil Menambah Data');
        }
        return redirect('/casts');
    }
    
    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        $cast = DB::table('casts')
            ->where('id', $id)
            ->update([
                'name' => $request['name'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]);
            if($cast){
                Session::flash('status','success');
                Session::flash('message','Berhasil Mengedit Data');
            }
        return redirect('/casts');
    }
    
    public function destroy($id)
    {
        $cast = DB::table('casts')->where('id', $id)->delete();
        if($cast){
            Session::flash('status','success');
            Session::flash('message','Berhasil Menghapus Data');
        }
        return redirect('/casts');
    }
}
