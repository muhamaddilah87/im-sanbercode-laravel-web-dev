@extends('layouts/master')

@section('judul')
    Admin LTE | Dashboard
@endsection
@section('title')
    Halaman Dashboard
@endsection

@section('sub-title')
    Dashboard
@endsection

@section('content')
<div class="d-flex align-items-center justify-content-center" style="height: 250px;">
    <p class="p-2 m-2 bg-primary text-white shadow rounded-sm">Ini Halaman Dashboard</p>
</div>
@endsection