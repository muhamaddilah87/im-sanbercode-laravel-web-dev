@extends('layouts.master')

@section('judul')
    Admin LTE | Detail Data Cast
@endsection
@section('title')
    Halaman Detail Data Cast 
@endsection

@section('sub-title')
    Detail Data Cast
@endsection


@section('content')
<table id="example1" class="table table-bordered table-striped">
        <tr>
            <th>Nama</th>
            <td>{{$cast->name}}</td>   
        </tr>
        <tr>
            <th>Umur</th>  
            <td>{{$cast->umur}}</td>  
        </tr>
        <tr>
            <th>Bio</th>
            <td style="width: 90%">{{$cast->bio}}</td>
        </tr>
</table>
<div class="my-2">
    <a href="/casts" class="btn btn-success">Back</a>
</div>
@endsection