@extends('layouts.master')

@section('judul')
    Admin LTE | Data Cast
@endsection
@section('title')
    Halaman List Data Cast 
@endsection

@section('sub-title')
    Data Cast
@endsection

@push('cssDataTables')
<link href="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.css" rel="stylesheet"/>
@endpush

@push('script')
<script src="{{asset('/templating/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/templating/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@section('content')

    @if (Session::has('status'))
        <div class="alert alert-info my-2">
            {{Session::get('message')}}
        </div>
    @endif
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$value->name}}</td>
                <td>{{$value->umur}} Tahun</td>
                <td>{{$value->bio}}</td>
                <td class="d-flex flex-wrap">
                    <a href="/casts/{{$value->id}}" class="btn btn-primary mx-1">Detail</a>
                    <a href="/casts/{{$value->id}}/edit" class="btn btn-success mx-1">Edit</a>
                    <form action="/casts/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger mx-1">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
            @endforelse    
        </tbody>
    </table>

@endsection