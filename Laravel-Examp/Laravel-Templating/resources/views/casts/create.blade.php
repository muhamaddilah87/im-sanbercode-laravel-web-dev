@extends('layouts.master')

@section('judul')
    Admin LTE | Create Data Cast
@endsection
@section('title')
    Halaman Create Data Cast 
@endsection

@section('sub-title')
    Create Data Cast
@endsection


@section('content')
<form action="/casts" method="POST">
    @csrf
    <div class="form-group">
      <label for="name">Nama</label>
      <input type="text" class="form-control" name="name" id="name">
        @error('name')
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur">
          @error('umur')
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
          @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" rows="3" name="bio" id="bio"></textarea>
          @error('bio')
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
          @enderror
      </div>
    <a href="/casts" class="btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection