@extends('layouts.master')

@section('judul')
    Admin LTE | Edit Data Cast
@endsection
@section('title')
    Halaman Edit Data Cast 
@endsection

@section('sub-title')
    Edit Data Cast
@endsection


@section('content')
<form action="/casts/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="name">Nama</label>
      <input type="text" value="{{$cast->name}}" class="form-control" name="name" id="name">
        @error('name')
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" value="{{$cast->umur}}" class="form-control" name="umur" id="umur">
          @error('umur')
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
          @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" rows="3" id="bio">{{$cast->bio}}</textarea>   
        @error('bio')
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
          @enderror
      </div>
    <div>
        <a href="/casts" class="btn btn-success">Back</a>
        <button type="submit" class="btn btn-primary">Edit</button>
    </div>
  </form>
@endsection