<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Laravel Web Statis</title>
  </head>
  <html>
    <h1>Buat Akun Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="post">
        @csrf
        <label for="">First Name :</label><br>
        <input type="text" name="fname"><br>
        <label for="">Last Name :</label><br>
        <input type="text" name="lname">
    <div>
        <p>Gender:</p>
        <div >
            <input type="radio" id="Male" name="gender" value="Male">
            <label for="Male">Male</label><br>
            <input type="radio" id="Female" name="gender" value="Female">
            <label for="Female">Female</label><br>
            <input type="radio" id="Other" name="gender" value="Other">
            <label for="Other">Other</label>
          </div>
    </div>
    <div>
        <p>Nasionality :</p>
        <div>
            <select name="" id="">
                <option value="">Chose one..</option>
                <option value="Indoensia">Indoensia</option>
                <option value="Singapura">Singapura</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Australia">Australia</option>
            </select>
        </div>
    </div>
    <div>
        <p>Language Spoken</p>
        <div >
            <input type="checkbox">
            <label for="">Bahasa Indonesia</label><br>
            <input type="checkbox">
            <label for="">Bahasa Inggris</label><br>
            <input type="checkbox" >
            <label for="">Other</label>
          </div>
    </div>
    <div>
        <p>BIO :</p>
        <textarea cols="30" rows="10"></textarea><br>
    </div>
    <button type="submit">Submit</button>
    </form>
  </body>
</html>
