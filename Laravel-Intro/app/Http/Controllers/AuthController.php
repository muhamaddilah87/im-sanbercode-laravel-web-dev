<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        // dd($request);
        $FName = $request['fname'];
        $LName = $request['lname'];
        return view('welcome',['FName' => $FName, 'LName' => $LName]);
    }
}
